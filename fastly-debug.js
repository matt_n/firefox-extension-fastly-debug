"use strict";

function addFastlyDebugHeader(e) {
    e.requestHeaders.push({'name':'Fastly-Debug', 'value':'1'});
    return { requestHeaders: e.requestHeaders };
}

browser.webRequest.onBeforeSendHeaders.addListener(
  addFastlyDebugHeader,
  { urls: [ "<all_urls>" ] },
  ["blocking", "requestHeaders"]
);